<?php

namespace App\Http\Controllers\Api;

use App\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{

	public function index(){
    $exam = Exam::with('questions', 'user', 'module')->get();
    return $exams;
	}

	public function show($id) {
		$exam = Exam::with('module')->find($id);

		if ($exam) {
			return $exam;
		} else {
			return response()->json([
				'message' => 'Record not found',
			], 404);
		}	
	}

	public function destroy($id) {
		$exam = Exam::find($id);
		if (!$exam) {
			return response()->json([
				'message'=> 'Event not find'
			], 404);
		}
		$exam->delete();
		return ['deleted' => $id];
	}

}
