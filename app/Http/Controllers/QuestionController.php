<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Module;

class QuestionController extends Controller
{
    public function index() {
    	$questions = Question::paginate();
    	return view('questions.index', ['questions' => $questions]);
    }

    public function create() {

    	$modules = Module::all();
    	return view('questions.create', ['modules' => $modules]);
    }

    public function store(Request $request) {

        $this->validate($request, [
            'text' => 'required|max:255',
            'a' => 'required|max:25',
            'b' => 'required|max:25',
            'c' => 'required|max:25',
            'd' => 'required|max:25',
            'answer' => 'required|max:1',
            'module_id' => 'required'

        ]);

    	$question = new Question( $request->all() );
        $question->save();
        return redirect('/questions');
    }

    
}
