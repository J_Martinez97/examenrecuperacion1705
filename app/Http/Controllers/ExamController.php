<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use Auth;
use Session;



class ExamController extends Controller
{
    public function index() {
    	$exams = Exam::paginate();
    	return view('exams.index', [ 'exams'=>$exams ]);
    }

    public function destroy($id) {
    	$exam = Exam::where('id', $id)->first();
    	$this->authorize('delete', $exam);
    	Exam::destroy($id);
    	return redirect("/exams");
    }

    public function remember($id) {
        $exam = Exam::where('id', $id)->first();
        Session::put('exam', $exam);
        return redirect("/exams");
    }

    public function forget () {
        Session::forget('exam');
        return redirect("/exams");
    }
}
