<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['text', 'a', 'b', 'c', 'd', 'answer', 'module_id'];


	public function exams(){
		return $this->belongsToMany('App\Exam');
	}

	public function module(){
		return $this->belongsTo('App\Module');
	}
}