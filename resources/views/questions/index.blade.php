@extends('layouts.app')
@section('content')

<div class="container">
	
	<h1>Lista preguntas</h1>

	<table class="table table-bordered">
		
		<tr>
			<th>Preguntas</th>
			<th>A</th>
			<th>B</th>
			<th>C</th>
			<th>D</th>
			<th>Respuestas</th>
			<th>Modulos</th>
		</tr>

		@foreach($questions as $question)
		<tr>
			<td> {{ $question->text }}</td>

			@if ($question->answer == 'a')
				<td class="bg-success">
			@else
				<td>
			@endif
			{{ $question->a }}</td>

			@if ($question->answer == 'b')
				<td class="bg-success">
			@else
				<td>
			@endif
			{{ $question->b }}</td>

			@if ($question->answer == 'c')
				<td class="bg-success">
			@else
				<td>
			@endif
			{{ $question->c }}</td>

			@if($question->answer == 'd')
			<td class="bg-success">
			@else
			<td>
			@endif
			{{ $question->d }}</td>

			<td> {{ $question->answer }}</td>
			<td> {{ $question->module->name }}</td>
		</tr>

		@endforeach
	</table>
{{ $questions->links() }}
</div>

@endsection
