@extends('layouts.app')
@section('content')

<div class="container">
	<h1>Alta Preguntas</h1>

<form method="post" action="/questions">
	{{ csrf_field() }}
	<div class="form-group">
		<label>Texto</label>
		<input type="text" name="text" class="form-control" value="{{ old('text') }}">
	</div>	

	<div class="form-group">
		<label>A</label>
		<input type="text" name="a" class="form-control" value="{{ old('a') }}">
	</div>	
	<div class="form-group">
		<label>B</label>
		<input type="text" name="b" class="form-control" value="{{ old('b') }}">
	</div>	
	<div class="form-group">
		<label>C</label>
		<input type="text" name="c" class="form-control" value="{{ old('c') }}">
	</div>	

	<div class="form-group">
		<label>D</label>
		<input type="text" name="d" class="form-control" value="{{ old('d') }}">
	</div>

	<div class="form-group">
		<label>Respuesta</label>
		<input type="text" name="answer" class="form-control" value="{{ old('d') }}">
	</div>

	<div class="form-group">
		<label>Modulo</label>
		<select class="form-control" name="module_id">
			@foreach ($modules as  $module)
				<option value="{{ $module->id }}">{{ $module->name }}</option>
			@endforeach
		</select>
	</div>

	 <div class="alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>

	<div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>
	</div>
	</form>
</div>
@endsection
