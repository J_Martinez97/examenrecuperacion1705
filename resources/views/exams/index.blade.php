@extends('layouts.app')
@section('content')
@auth


<div class="container">
	<h1>Lista Examenes</h1>

	@if (Session::has('exam'))
		<h3>Recordatorio Examen</h3>
		{{ Session::get('exam')->title }}
		<a class="btn btn-warning" href="/exams/forget">Olvidar</a>
	@endif
	
	<table class="table table-bordered">
		<tr>
			<th>Titulo</th>
			<th>Fecha</th>
			<th>Creador</th>
			<th>Modulo</th>
			<th>Acciones</th>
		</tr>

		 @foreach ($exams as $exam)
		<tr>
			<td>{{ $exam->title }}</td>
			<td>{{ $exam->date }}</td>
			<td>{{ $exam->user->name }}</td>
			<td>{{ $exam->module->name }}</td>
			<td>
				<form method="post" action="/exams/{{ $exam->id }}">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="delete">
					@can ( 'delete', $exam )
					<input class="btn btn-danger" type="submit" name="Borrar" value="Borrar">
					@endcan

					<a class="btn btn-secondary" href="/exams/{{ $exam->id }}/remember">Remember</a>

				</form>
			</td>
		</tr>
		 @endforeach
	</table>

	{{ $exams->links() }}
</div>

<hr>
@endauth

@guest
<h3>Debe estar logueado para acceder aqui</h3>
@endguest

@endsection